﻿using UnityEngine;
using System.Collections;

public class AddForceAtStart : MonoBehaviour {

	public Vector2 force;

	void Start () {
		rigidbody2D.AddForce(force);
		rigidbody2D.AddTorque(-.5F);
	}
}
