﻿using UnityEngine;
using System.Collections;

public class AttractToPlanet : MonoBehaviour {

	public Transform otherObject;
	public float force;

	private Vector2 difference;
	public bool landed;

	void Update () {
		if (!landed) {
			difference = transform.position - otherObject.position;
			rigidbody2D.AddForce(-difference.normalized * rigidbody2D.mass / difference.sqrMagnitude, ForceMode2D.Impulse);
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (col.collider)
			landed = true;
	}

	void OnCollisionExit2D () {
		landed = false;
	}
}
